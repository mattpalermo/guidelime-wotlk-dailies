Guidelime.registerGuide(
[[
[GA Alliance]
[N - Sons of Hodir]
[D This guide was created by Ardo for Sons of  Hodir Dailies. You must complete the Sons of Hodir quest line in The Storm Peaks first!]
This guide was created by Ardo for Sons of  Hodir Dailies. You must complete the Sons of Hodir quest line in The Storm Peaks first!

Fly to Dun Niffelem [G 62.6,60.9 The Storm Peaks][O]
Accept: [QA12981]
Accept: [QA12977]
Accept: [QA13003]
Accept: [QA12994]
Accept: [QA13006]
Accept: [QA13046][O] only if you're Revered with Sons of Hodir.

Kill Brittle Revenants and loot 6 [CI42246,6 Essence of Ice]
Do: [QC12981]
Do: [QC12977]
Do: [QC12994]
Do: [QC13046]
Do: [QC13006]
Do: [QC13003]

If you have 10 or more Relics of Ulduar, turn in Hodir's Tribute: [QT13559][O]
Turn in: [QT12981]
Turn in: [QT12977]
Turn in: [QT13003]
Turn in: [QT12994]
Turn in: [QT13006]
Turn in: [QT13046]


]], "Ardo's WOTLK Dailies")