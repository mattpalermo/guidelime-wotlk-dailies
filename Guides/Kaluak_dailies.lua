Guidelime.registerGuide(
[[
[GA Alliance]
[N - Kalu'ak]
[D This guide was created by Ardo for Kalu'ak Dailies. ]
This guide was created by Ardo for Kalu'ak Dailies.

Fly to Kamagua [G 25.3,58.3 Howling Fjord][O]
Accept: [QA11472]
Look for a School of Tasty Reef fish near [G 29.9,75.0 Howling Fjord]
Use the [UI40946 Anuniaq's Net] to collect 1  [CI34127,1 Tasty Reef Fish]
Go across the water and look for Reef Bulls [G 31.9,74.3 Howling Fjord]
Smack a Reef Bull but don't do too much damage. Then kite it across the water toward the Reef Cows. When you get the Reef Bull close to a Reef Cow, use a [UI34127,1 Tasty Reef Fish] to complete the quest [QC11472]
Turn in: [QT11472]

Fly to Moa'ki Harbor [G 48.3,74.3 Dragonblight][O]
Accept: [QA11960]
Do: [QC11960]
Turn in: [QT11960]

Fly to Kaskala [G 64.0,45.6 Borean Tundra][O]
Accept: [QA11945]
Do: [QC11945]
Turn in: [QT11945]

]], "Ardo's WOTLK Dailies")